<?php
include('db.php');
$ipclock = '';
$description= '';

if  (isset($_GET['id'])) {
  $id = $_GET['id'];
  $query = "SELECT * FROM Clock WHERE Id_Clock=$id";
  $result = mysqli_query($conn, $query);
  $row_cnt = mysqli_num_rows($result);
  if ($row_cnt == 1) {
    $row = mysqli_fetch_array($result);
    $ipclock = $row['Ip_clock'];
    $description = $row['Location_clock'];
  }
}

if (isset($_POST['update'])) {
  $id = $_GET['id'];
  $ipclock= $_POST['ip'];
  $description = $_POST['description'];

  $query = "select * from Clock where Ip_Clock='$ipclock'";
  $result = mysqli_query($conn, $query);
  $row_cnt = mysqli_num_rows($result);
  if ($row_cnt==0){
    $query = "UPDATE Clock set Location_clock = '$description', Ip_clock = '$ipclock' WHERE id_Clock='$id'";
    mysqli_query($conn, $query);
    $_SESSION['message'] = 'Reloj actualizado satisfactoriamente';
    $_SESSION['message_type'] = 'success';
    header('Location: index.php');
  }
  else{
    $_SESSION['message'] = 'Reloj Duplicado';
    $_SESSION['message_type'] = 'danger';
    header('Location: index.php');
  }
}

?>
<?php include('includes/header.php'); ?>
<div class="container p-4">
  <div class="row">
    <div class="col-md-4 mx-auto">
      <div class="card card-body">
      <form action="edit.php?id=<?php echo $_GET['id']; ?>" method="POST" name="principal" >
        <div class="form-group" >
          <input name="ip" type="text" class="form-control" value="<?php echo $ipclock; ?>" placeholder="IP del Reloj" onchange="ValidateIPaddress(document.principal.ip)" >
        </div>
        <script src="js/ipaddress-validation.js"></script>
        <div class="form-group">
        <textarea name="description" class="form-control" cols="30" rows="10"><?php echo $description;?></textarea>
        </div>
        <button class="btn-success" name="update">Actualizar</button>
      </form>
      </div>
    </div>
  </div>
</div>
<?php include('includes/footer.php'); ?>
